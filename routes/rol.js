// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de rol
var rol_controller = require('../controllers/rol');

// GET /:id
router.get('/:id', rol_controller.rol_details);
// GET /
router.get('/', rol_controller.rol_all);
// POST /
router.post('/', rol_controller.rol_create);
// PUT /:id
router.put('/:id', rol_controller.rol_update);
// DELETE /:id
router.delete('/:id', rol_controller.rol_delete);

//export router
module.exports = router;