var superagent = require('superagent')
var expect = require('expect.js')

describe('Test  ubicacion', function () {
  var id
  var url = 'http://localhost:3001';
  // GET /


  // ------------------Pruebas a la ruta ubicacion------------------

  // POST /ubicacion
  it('Test POST /ubicacion ', function (done) {
    superagent.post(url + '/ubicacion')
      .send({
        nombre: "Piso 5" , 
        descripcion:"Laboratoriol piso 5",
        direccion: "Calle 123"
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /ubicacion
  it('Test GET /ubicacion', function (done) {
    superagent.get(url + '/ubicacion')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /ubicacion/:id
 it('test GET /ubicacion/:id', function (done) {
  superagent.get(url + '/ubicacion/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /ubicacion/:id
it('Test PUT /ubicacion/:id ', function (done) {
  superagent.put(url + '/ubicacion/'+id)
    .send({
      nombre: "Piso 6" , 
        descripcion:"Laboratoriol piso 6",
        direccion: "Calle 123"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.ubicacion.nombre).to.eql('Piso 6')
      done()
    })
})

// DELETE /ubicacion/:id
it('Test DELETE /ubicacion/:id ', function (done) {
  superagent.delete(url +'/ubicacion/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
