//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Rol
var RolSchema = new Schema({
nombre: {type:String, required:true}, 
perfil: {type:String, required:true}, 
permisos: {type:String, required:true}, 

});

RolSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Rol', RolSchema);