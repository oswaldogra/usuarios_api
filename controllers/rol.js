//importar modelo
var Rol = require('../models/rol');

//funcion create
exports.rol_create = function (req, res) {
    var rol = new Rol(
        req.body
    );

    rol.save(function (err, rol) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
            
        }
        res.send({status: "created", id: rol._id})
    })
};
//funcion read by id
exports.rol_details = function (req, res) {
    Rol.findById(req.params.id, function (err, rol) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(rol);
    })
};
//funcion read all
exports.rol_all = function (req, res) {
    Rol.find(req.params.id, function (err, rol) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(rol);
    })
};
//funcion update
exports.rol_update = function (req, res) {
    Rol.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, rol) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status: "updated", rol: rol });
    });
};

//funcion delete
exports.rol_delete = function (req, res) {
    Rol.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status:"deleted"});
    })
};